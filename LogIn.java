import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LogIn {
	protected WebDriver driver;
	
	private By UserValid1 = By.id("user-name");
	//private By UserValid2 = By.id("user-name");
	//private By UserValid3 = By.id("user-name");
	//private By UserValid4 = By.id("user-name");
	private By Password = By.id("password");
	private By ButtonLogIn = By.id("login-button");
	
	//Shop
	private By Backpack = By.id("add-to-cart-sauce-labs-backpack");
	private By Onesie = By.id("add-to-cart-sauce-labs-onesie");
	
	
	//Shopping cart
	private By Shopping = By.id("shopping_cart_container");
	
	//Checkout
	private By Checkout = By.id("checkout");
	
	//Information
	private By FName = By.id("first-name");
	private By LName = By.id("last-name");
	private By Zip = By.id("postal-code");
	private By Continue = By.id("continue");
	
	//Finish
	private By end = By.id("finish");
	
	public LogIn() {
		this.driver= driver;
	}
			
		public void Fill(WebDriver driver) throws InterruptedException {
			
			//
		System.setProperty("webdriver.chrome.driver","C:\\Users\\LENOVO\\Desktop\\chromedriver_win32_97\\chromedriver.exe");
		
		driver.get("https://www.saucedemo.com/");
		
		//User and Password 
		driver.findElement(UserValid1).sendKeys("standard_user");
		//driver.findElement(UserValid2).sendKeys("locked_out_user");
		//driver.findElement(UserValid3).sendKeys("problem_user");
		//driver.findElement(UserValid4).sendKeys("performance_glitch_user");
		driver.findElement(Password).sendKeys("secret_sauce");
		Thread.sleep(1000);
		driver.findElement(ButtonLogIn).click();
		Thread.sleep(1000);
		
		
		//Shop
		driver.findElement(Backpack).click();
		Thread.sleep(1500);
		driver.findElement(Onesie).click();
		Thread.sleep(1500);
		
		
		//Shopping car
		driver.findElement(Shopping).click();
		Thread.sleep(1500);
		
		
		//Checkout
		driver.findElement(Checkout).click();
		Thread.sleep(1500);
		
		//Information
		driver.findElement(FName).sendKeys("Myriam");
		Thread.sleep(1500);
		driver.findElement(LName).sendKeys("Robles");
		Thread.sleep(1500);
		driver.findElement(Zip).sendKeys("44600");
		Thread.sleep(1500);
		driver.findElement(Continue).click();
		
		//Finish
		driver.findElement(end).click();
		Thread.sleep(1500);
		
		}
		
	

}




