import static org.junit.Assert.assertEquals;

import java.time.Duration;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LogInTest {
@SuppressWarnings({ "resource", "deprecation" })
@Test
	public void logInTest()throws Exception {
	System.setProperty("webdriver.chrome.driver","C:\\Users\\LENOVO\\Desktop\\chromedriver_win32_97\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.get("https://www.saucedemo.com/");
	
	LogIn LogIn = new LogIn();
	LogIn.Fill(driver);		
		
			
			WebDriverWait Wait= new WebDriverWait(driver,
					Duration.ofSeconds(5));
					Wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//h2[@class='complete_header']")));
					String expect =driver.findElement(By.tagName("h2")).getText();
					String expectedOutput= "THANK YOU FOR YOUR ORDER";
					assertEquals(expect,expectedOutput);
					System.out.println("Test Complete");
					

	}

}
